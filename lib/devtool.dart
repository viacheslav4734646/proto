import 'package:flutter/foundation.dart';
import 'dart:developer' as dev;

import 'package:proto/tree_node.dart';

class TreeBinding {
  TreeBinding._();

  static final debugInstance = kDebugMode
      ? TreeBinding._()
      : throw UnsupportedError('Can\'t use TreeBinding in release mode');

  List<TreeNode> _roots = [];
  List<TreeNode> get roots => [..._roots];
  set roots(List<TreeNode> value) {
    // post an event to devtool with new state, it should rebuild with new nodes
    dev.postEvent('proto:stateUpdated', {
      'roots': [...value.map((e) => e.toJson())]
    });
    print('Sent new roots to devtools:');
    print('${value.map((e) => e.toJson())}');
    _roots = value;
  }
}
