import 'dart:math';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:proto/devtool.dart';
import 'package:proto/tree_node.dart';
import 'dart:developer' as dev;

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Devtools Prototype',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: const MyHomePage(title: 'Devtools Prototype'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final initialNode = TreeNode(name: 'name1');
  final _roots = <TreeNode>[];

  final _rand = Random();

  int _topLevelNodeCount = 0;
  int _totalNodeCount = 0;

  var increment = 1;

  @override
  void initState() {
    super.initState();

    // Test values
    initialNode.children.addAll([
      TreeNode(name: 'name2'),
      TreeNode(name: 'name3', children: [
        TreeNode(name: 'name4', children: [
          TreeNode(name: 'name6'),
        ]),
        TreeNode(name: 'name5'),
      ]),
    ]);
    _roots.add(initialNode);

    // Account for intial values
    for (final _ in initialNode.allNodes) {
      increment++;
    }
  }

  int _calcTotalNodeCount() {
    var count = 0;
    for (final root in _roots) {
      count += root.allNodes.length;
    }
    return count;
  }

  void _updateCounters() {
    setState(() {
      _topLevelNodeCount = _roots.length;
      _totalNodeCount = _calcTotalNodeCount();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            ElevatedButton(
              onPressed: () {
                final newRoot = TreeNode(name: 'name${increment++}');
                // Fill new root with nodes
                for (var i = 0; i < 5; i++) {
                  final newRootNodes = newRoot.allNodes;
                  final randIndex = _rand.nextInt(newRootNodes.length);
                  newRootNodes[randIndex]
                      .children
                      .add(TreeNode(name: 'name${increment++}'));
                }

                _roots.add(newRoot);

                // Update tree binding
                if (kDebugMode) {
                  TreeBinding.debugInstance.roots = [..._roots];
                }

                _updateCounters();

                //ws://127.0.0.1:58044/riBSCw3XYm0=/ws
              },
              child: const Text('Add a new root'),
            ),
            const SizedBox(
              height: 25,
            ),
            Text(
              'Top level nodes: $_topLevelNodeCount',
              style: Theme.of(context).textTheme.headlineMedium,
            ),
            const SizedBox(height: 15),
            Text(
              'Total nodes: $_totalNodeCount',
              style: Theme.of(context).textTheme.headlineMedium,
            ),
          ],
        ),
      ),
    );
  }
}
