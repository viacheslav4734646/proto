class TreeNode {
  TreeNode({required this.name, children}) : children = children ?? [];

  final String name;
  final List<TreeNode> children;

  List<TreeNode> get allNodes {
    final nodes = <TreeNode>[];

    nodes.add(this);

    for (final child in children) {
      nodes.addAll(child.allNodes);
    }

    return nodes;
  }

  Map<String, dynamic> toJson() {
    return {
      'name': name,
      'children': [...children.map((child) => child.toJson())],
    };
  }

  factory TreeNode.fromJson(Map<String, dynamic> json) {
    final children = json['children'] as List;
    return TreeNode(
      name: json['name'],
      children: [...children.map((child) => TreeNode.fromJson(child))],
    );
  }
}
